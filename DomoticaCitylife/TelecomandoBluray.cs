﻿using System;
using iDigitalDomotica;

namespace DomoticaCitylife
{
	public class TelecomandoBluray
	{
		private Telecomando telecomando;
		public enum COMANDO { ACCENDI, PAUSA, PLAY, STOP, PREV, REV, FWD, NEXT, TOP_MENU, POPUP_MENU, LANGUAGE, SUBTITLES, FRECCIA_SU, FRECCIA_GIU, FRECCIA_SX, FRECCIA_DX, ENTER,
			OPEN, EXIT };


		public TelecomandoBluray (Stanze.STANZE stanza)
		{
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				telecomando = new Telecomando (Rete.LIVING_IP_PULCE_BLURAY, Rete.PORTA_PULCE_IR, Rete.LIVING_CANALE_PULCE_BLURAY);
				break;
			case Stanze.STANZE.CUCINA:
				//Non c'è bluray
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è bluray
				break;
			case Stanze.STANZE.EMANUELE:
				//Non c'è bluray
				break;
			}

			if (this.telecomando != null) {
				this.telecomando.ConnectTimeout = Rete.TIMEOUT;
				this.telecomando.SendTimeout = Rete.TIMEOUT;
				this.telecomando.ReceiveTimeout = Rete.TIMEOUT;
			}
		}



		public void InviaComandoIR(COMANDO comando){

			if (this.telecomando == null) return;

			string strComando = string.Empty;

			switch (comando) {

			case COMANDO.ACCENDI:
				strComando = iDigitalDomotica.TelecomandoOppo.ACCENDI;
				break;
			case COMANDO.PAUSA:
				strComando = iDigitalDomotica.TelecomandoOppo.PAUSA;
				break;
			case COMANDO.PLAY:
				strComando = iDigitalDomotica.TelecomandoOppo.PLAY;
				break;
			case COMANDO.STOP:
				strComando = iDigitalDomotica.TelecomandoOppo.STOP;
				break;
			case COMANDO.PREV:
				strComando = iDigitalDomotica.TelecomandoOppo.PREV;
				break;
			case COMANDO.REV:
				strComando = iDigitalDomotica.TelecomandoOppo.REV;
				break;
			case COMANDO.FWD:
				strComando = iDigitalDomotica.TelecomandoOppo.FWD;
				break;
			case COMANDO.NEXT:
				strComando = iDigitalDomotica.TelecomandoOppo.NEXT;
				break;
			case COMANDO.TOP_MENU:
				strComando = iDigitalDomotica.TelecomandoOppo.TOP_MENU;
				break;
			case COMANDO.POPUP_MENU:
				strComando = iDigitalDomotica.TelecomandoOppo.POPUP_MENU;
				break;
			case COMANDO.LANGUAGE:
				strComando = iDigitalDomotica.TelecomandoOppo.LINGUA;
				break;
			case COMANDO.SUBTITLES:
				strComando = iDigitalDomotica.TelecomandoOppo.SOTTOTITOLI;
				break;
			case COMANDO.FRECCIA_SU:
				strComando = iDigitalDomotica.TelecomandoOppo.FRECCIA_SU;
				break;
			case COMANDO.FRECCIA_GIU:
				strComando = iDigitalDomotica.TelecomandoOppo.FRECCIA_GIU;
				break;
			case COMANDO.FRECCIA_SX:
				strComando = iDigitalDomotica.TelecomandoOppo.FRECCIA_SX;
				break;
			case COMANDO.FRECCIA_DX:
				strComando = iDigitalDomotica.TelecomandoOppo.FRECCIA_DX;
				break;
			case COMANDO.ENTER:
				strComando = iDigitalDomotica.TelecomandoOppo.ENTER;
				break;
			case COMANDO.OPEN:
				strComando = iDigitalDomotica.TelecomandoOppo.OPEN;
				break;
			case COMANDO.EXIT:
				//Non abbiamo il comando
				break;
			}

			telecomando.InviaComandoIR (strComando);
		}
	}
}