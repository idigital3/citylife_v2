﻿using System;

namespace DomoticaCitylife
{
	public class Rete
	{
		//public Rete () { }

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///**************************************** CONFIGURAZIONE RETE ****************************************
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//Tempo in ms di timeout per connessione, invio e ricezione risposta dai vari dispositivi.
		public static int TIMEOUT = 1500;

		//Ip e porta del dispositivo soundweb 100 bss
		public static string 	SOUNDWEB_100BSS_IP 				= "192.168.1.31";
		public static int 		SOUNDWEB_100BSS_PORTA 			= 1023;

		//Porta pulce IR e porta sensore tv
		public static int 		PORTA_PULCE_IR 					= 4998;
		public static int 		PORTA_SENSORE_TV				= 5000;

		//Porta matrice video Kramer
		public static int 		PORTA_KRAMER					= 5000;

		//LIVING
		public static string 	LIVING_IP_PULCE_TV 				= "192.168.1.44";
		public static string 	LIVING_IP_PULCE_SKY  			= "192.168.1.41";
		public static string 	LIVING_IP_PULCE_MYSKY  			= "192.168.1.41";
		public static string 	LIVING_IP_PULCE_APPLETV  		= "192.168.1.41";
		public static string 	LIVING_IP_PULCE_DIGITALE  		= "192.168.1.62";
		public static string 	LIVING_IP_PULCE_BLURAY  		= "192.168.1.44";
		public static string 	LIVING_IP_SENSORE_TV  			= "192.168.1.48";
		public static string 	LIVING_IP_KRAMER				= "192.168.1.37";
		public static uint 		LIVING_CANALE_PULCE_TV 			= 1;
		public static uint 		LIVING_CANALE_PULCE_SKY 		= 1;
		public static uint 		LIVING_CANALE_PULCE_MYSKY 		= 1;
		public static uint 		LIVING_CANALE_PULCE_APPLETV 	= 2;
		public static uint 		LIVING_CANALE_PULCE_DIGITALE 	= 1;
		public static uint 		LIVING_CANALE_PULCE_BLURAY 		= 3;

		//CUCINA
		public static string 	CUCINA_IP_PULCE_TV 				= "192.168.1.45";
		public static string 	CUCINA_IP_PULCE_SKY 			= "192.168.1.42";
		public static string 	CUCINA_IP_PULCE_MYSKY 			= "192.168.1.41";
		public static string 	CUCINA_IP_PULCE_APPLETV 		= "192.168.1.42";
		public static string 	CUCINA_IP_PULCE_DIGITALE 		= "192.168.1.62";
		public static string 	CUCINA_IP_SENSORE_TV  			= "192.168.1.49";
		public static string 	CUCINA_IP_KRAMER				= "192.168.1.38";
		public static uint 		CUCINA_CANALE_PULCE_TV 			= 1;
		public static uint 		CUCINA_CANALE_PULCE_SKY 		= 1;
		public static uint 		CUCINA_CANALE_PULCE_MYSKY 		= 1;
		public static uint 		CUCINA_CANALE_PULCE_APPLETV 	= 2;
		public static uint 		CUCINA_CANALE_PULCE_DIGITALE 	= 2;

		//EMANUELA - NON HA TV
		public static string 	EMANUELA_IP_PULCE_TV 			= "";
		public static string 	EMANUELA_IP_SENSORE_TV  		= "";
		public static uint 		EMANUELA_CANALE_PULCE_TV 		= 1;

		//EMANUELE
		public static string 	EMANUELE_IP_PULCE_TV 			= "192.168.1.46";
		public static string 	EMANUELE_IP_PULCE_MYSKY 		= "192.168.1.41";
		public static string 	EMANUELE_IP_PULCE_APPLETV 		= "192.168.1.43";
		public static string 	EMANUELE_IP_PULCE_DIGITALE 		= "192.168.1.62";
		public static string 	EMANUELE_IP_SENSORE_TV  		= "192.168.1.50";
		public static string 	EMANUELE_IP_KRAMER		  		= "192.168.1.39";
		public static uint 		EMANUELE_CANALE_PULCE_TV 		= 1;
		public static uint 		EMANUELE_CANALE_PULCE_MYSKY 	= 1;
		public static uint 		EMANUELE_CANALE_PULCE_APPLETV 	= 2;
		public static uint 		EMANUELE_CANALE_PULCE_DIGITALE 	= 3;
	}
}