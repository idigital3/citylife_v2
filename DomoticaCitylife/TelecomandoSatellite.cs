﻿using System;
using iDigitalDomotica;

namespace DomoticaCitylife
{
	public class TelecomandoSatellite
	{
		private Telecomando telecomando;
		public enum COMANDO { NUM_0, NUM_1, NUM_2, NUM_3, NUM_4, NUM_5, NUM_6, NUM_7, NUM_8, NUM_9, CANALE_PIU, CANALE_MENO, OK };

		public TelecomandoSatellite (Stanze.STANZE stanza)
		{
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				telecomando = new Telecomando (Rete.LIVING_IP_PULCE_DIGITALE, Rete.PORTA_PULCE_IR, Rete.LIVING_CANALE_PULCE_DIGITALE);
				break;
			case Stanze.STANZE.CUCINA:
				telecomando = new Telecomando (Rete.CUCINA_IP_PULCE_DIGITALE, Rete.PORTA_PULCE_IR, Rete.CUCINA_CANALE_PULCE_DIGITALE);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				telecomando = new Telecomando (Rete.EMANUELE_IP_PULCE_DIGITALE, Rete.PORTA_PULCE_IR, Rete.EMANUELE_CANALE_PULCE_DIGITALE);
				break;
			}

			if (this.telecomando != null) {
				this.telecomando.ConnectTimeout = Rete.TIMEOUT;
				this.telecomando.SendTimeout = Rete.TIMEOUT;
				this.telecomando.ReceiveTimeout = Rete.TIMEOUT;
			}
		}


		public void InviaComandoIR(COMANDO comando){

			if (this.telecomando == null) return;

			string strComando = string.Empty;

			switch (comando) {

			case COMANDO.NUM_0:
				strComando = iDigitalDomotica.TelecomandoIDSat.ZERO;
				break;
			case COMANDO.NUM_1:
				strComando = iDigitalDomotica.TelecomandoIDSat.UNO;
				break;
			case COMANDO.NUM_2:
				strComando = iDigitalDomotica.TelecomandoIDSat.DUE;
				break;
			case COMANDO.NUM_3:
				strComando = iDigitalDomotica.TelecomandoIDSat.TRE;
				break;
			case COMANDO.NUM_4:
				strComando = iDigitalDomotica.TelecomandoIDSat.QUATTRO;
				break;
			case COMANDO.NUM_5:
				strComando = iDigitalDomotica.TelecomandoIDSat.CINQUE;
				break;
			case COMANDO.NUM_6:
				strComando = iDigitalDomotica.TelecomandoIDSat.SEI;
				break;
			case COMANDO.NUM_7:
				strComando = iDigitalDomotica.TelecomandoIDSat.SETTE;
				break;
			case COMANDO.NUM_8:
				strComando = iDigitalDomotica.TelecomandoIDSat.OTTO;
				break;
			case COMANDO.NUM_9:
				strComando = iDigitalDomotica.TelecomandoIDSat.NOVE;
				break;
			case COMANDO.CANALE_PIU:
				strComando = iDigitalDomotica.TelecomandoIDSat.CANALE_PIU;
				break;
			case COMANDO.CANALE_MENO:
				strComando = iDigitalDomotica.TelecomandoIDSat.CANALE_MENO;
				break;
			case COMANDO.OK:
				strComando = iDigitalDomotica.TelecomandoIDSat.OK;
				break;
			}

			telecomando.InviaComandoIR (strComando);
		}
	}
}