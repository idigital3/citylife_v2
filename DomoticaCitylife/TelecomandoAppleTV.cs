﻿using System;
using iDigitalDomotica;

namespace DomoticaCitylife
{
	public class TelecomandoAppleTV
	{
		private Telecomando telecomando;

		public enum COMANDO { FRECCIA_SU, FRECCIA_GIU, FRECCIA_DX, FRECCIA_SX, ENTER, MENU, PLAY_PAUSA };

		public TelecomandoAppleTV (Stanze.STANZE stanza)
		{
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				telecomando = new Telecomando (Rete.LIVING_IP_PULCE_APPLETV, Rete.PORTA_PULCE_IR, Rete.LIVING_CANALE_PULCE_APPLETV);
				break;
			case Stanze.STANZE.CUCINA:
				telecomando = new Telecomando (Rete.CUCINA_IP_PULCE_APPLETV, Rete.PORTA_PULCE_IR, Rete.CUCINA_CANALE_PULCE_APPLETV);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				telecomando = new Telecomando (Rete.EMANUELE_IP_PULCE_APPLETV, Rete.PORTA_PULCE_IR, Rete.EMANUELE_CANALE_PULCE_APPLETV);
				break;
			}

			if (this.telecomando != null) {
				this.telecomando.ConnectTimeout = Rete.TIMEOUT;
				this.telecomando.SendTimeout = Rete.TIMEOUT;
				this.telecomando.ReceiveTimeout = Rete.TIMEOUT;
			}
		}


		public void InviaComandoIR(COMANDO comando){

			if (this.telecomando == null) return;

			string strComando = string.Empty;

			switch (comando) {

			case COMANDO.FRECCIA_SU:
				strComando = iDigitalDomotica.TelecomandoAppleTV.UP;
				break;
			case COMANDO.FRECCIA_GIU:
				strComando = iDigitalDomotica.TelecomandoAppleTV.DOWN;
				break;
			case COMANDO.FRECCIA_DX:
				strComando = iDigitalDomotica.TelecomandoAppleTV.RIGHT;
				break;
			case COMANDO.FRECCIA_SX:
				strComando = iDigitalDomotica.TelecomandoAppleTV.LEFT;
				break;
			case COMANDO.ENTER:
				strComando = iDigitalDomotica.TelecomandoAppleTV.OK;
				break;
			case COMANDO.MENU:
				strComando = iDigitalDomotica.TelecomandoAppleTV.MENU;
				break;
			case COMANDO.PLAY_PAUSA:
				strComando = iDigitalDomotica.TelecomandoAppleTV.PLAY;
				break;
			
			}

			telecomando.InviaComandoIR (strComando);
		}
	}
}