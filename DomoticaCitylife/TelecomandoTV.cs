﻿using System;
using iDigitalDomotica;

namespace DomoticaCitylife
{
	public class TelecomandoTV
	{
		private Stanze.STANZE stanza;
		private Telecomando telecomando;

		public TelecomandoTV (Stanze.STANZE stanza)
		{
			this.stanza = stanza;

			switch (stanza) {
			case Stanze.STANZE.LIVING:
				telecomando = new Telecomando (Rete.LIVING_IP_PULCE_TV, Rete.PORTA_PULCE_IR, Rete.LIVING_CANALE_PULCE_TV, Rete.LIVING_IP_SENSORE_TV, Rete.PORTA_PULCE_IR, TelecomandoLgTV.ON_OFF);
				break;
			case Stanze.STANZE.CUCINA:
				telecomando = new Telecomando (Rete.CUCINA_IP_PULCE_TV, Rete.PORTA_PULCE_IR, Rete.CUCINA_CANALE_PULCE_TV, Rete.CUCINA_IP_SENSORE_TV, Rete.PORTA_PULCE_IR, TelecomandoSamsungTV.POWER);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				telecomando = new Telecomando (Rete.EMANUELE_IP_PULCE_TV, Rete.PORTA_PULCE_IR, Rete.EMANUELE_CANALE_PULCE_TV, Rete.EMANUELE_IP_SENSORE_TV, Rete.PORTA_PULCE_IR, TelecomandoSamsungTV.POWER);
				break;
			}

			if (this.telecomando != null) {
				this.telecomando.ConnectTimeout = Rete.TIMEOUT;
				this.telecomando.SendTimeout = Rete.TIMEOUT;
				this.telecomando.ReceiveTimeout = Rete.TIMEOUT;
			}
		}

		public void AccendiTv(){
			if (this.telecomando == null) return;
			this.telecomando.Accendi ();
		}

		public void SpegniTv(){
			if (this.telecomando == null) return;
			this.telecomando.Spegni ();
		}

		public void ToggleStatoAccensione(){
			if (this.telecomando == null) return;
			this.telecomando.ToggleStatoTV ();
		}

		public void VolumePiu(){
			if (this.telecomando == null) return;
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				this.telecomando.InviaComandoIR (TelecomandoLgTV.VOLUME_PIU);
				break;
			case Stanze.STANZE.CUCINA:
				this.telecomando.InviaComandoIR (TelecomandoSamsungTV.VOLUME_PIU);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				this.telecomando.InviaComandoIR (TelecomandoSamsungTV.VOLUME_PIU);
				break;
			}
		}


		public void VolumeMeno(){
			if (this.telecomando == null) return;
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				this.telecomando.InviaComandoIR (TelecomandoLgTV.VOLUME_MENO);
				break;
			case Stanze.STANZE.CUCINA:
				this.telecomando.InviaComandoIR (TelecomandoSamsungTV.VOLUME_MENO);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				this.telecomando.InviaComandoIR (TelecomandoSamsungTV.VOLUME_MENO);
				break;
			}
		}


		public void ToggleMuto(){
			if (this.telecomando == null) return;
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				this.telecomando.InviaComandoIR (TelecomandoLgTV.MUTE);
				break;
			case Stanze.STANZE.CUCINA:
				this.telecomando.InviaComandoIR (TelecomandoSamsungTV.MUTE);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				this.telecomando.InviaComandoIR (TelecomandoSamsungTV.MUTE);
				break;
			}
		}
	}
}