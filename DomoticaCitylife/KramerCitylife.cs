﻿using System;
using iDigitalDomotica;

namespace DomoticaCitylife
{
	public class KramerCitylife
	{
		private Kramer kramer;
		public enum SORGENTE_VIDEO { SKY, MYSKY, APPLETV, DIGITALE, BLURAY }

		public KramerCitylife (Stanze.STANZE stanza)
		{
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				kramer = new Kramer (Rete.LIVING_IP_KRAMER , Rete.PORTA_KRAMER);
				break;
			case Stanze.STANZE.CUCINA:
				kramer = new Kramer (Rete.CUCINA_IP_KRAMER, Rete.PORTA_KRAMER);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				kramer = new Kramer (Rete.EMANUELE_IP_KRAMER, Rete.PORTA_KRAMER);
				break;
			}

			if (this.kramer != null) {
				this.kramer.ConnectTimeout = Rete.TIMEOUT;
				this.kramer.SendTimeout = Rete.TIMEOUT;
				this.kramer.ReceiveTimeout = Rete.TIMEOUT;
			}
		}


		public void SelezionaSorgenteVideo(SORGENTE_VIDEO sorgente){

			if (this.kramer == null) return;

			switch (sorgente) {
			case SORGENTE_VIDEO.APPLETV:
				this.kramer.SelezionaSorgente (Kramer.VIDEO_SOURCE.Input3_out1);
				break;
			case SORGENTE_VIDEO.SKY:
				this.kramer.SelezionaSorgente (Kramer.VIDEO_SOURCE.Input2_out1);
				break;
			case SORGENTE_VIDEO.MYSKY:
				this.kramer.SelezionaSorgente (Kramer.VIDEO_SOURCE.Input1_out1);
				break;
			case SORGENTE_VIDEO.DIGITALE:
				this.kramer.SelezionaSorgente (Kramer.VIDEO_SOURCE.Input4_out1);
				break;
			case SORGENTE_VIDEO.BLURAY:
				//Viene selezionato in automatico
				break;
			}
		}
	}
}