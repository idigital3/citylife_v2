﻿using System;

namespace DomoticaCitylife
{
	public class ControllerSorgentiMultimediali
	{
		public enum SORGENTE { SKY, MYSKY, APPLETV, DIGITALE, BLURAY, AIRPLAY };  

		private KramerCitylife kramer;
		private SoundWebBlu100BSS soundweb;

		public ControllerSorgentiMultimediali (Stanze.STANZE stanza)
		{
			//Kramer gestisce lo switch delle sorgenti video
			this.kramer = new KramerCitylife (stanza);

			//Soundweb gestisce lo switch delle sorgenti audio
			this.soundweb = new SoundWebBlu100BSS(stanza);
		}

		public void SelezionaSorgente(SORGENTE preset){

			switch (preset) {
			case SORGENTE.SKY:
				this.kramer.SelezionaSorgenteVideo (KramerCitylife.SORGENTE_VIDEO.SKY);
				this.soundweb.SelezionaPresetAudio (SoundWebBlu100BSS.PRESET.SKY);
				break;
			case SORGENTE.MYSKY:
				this.kramer.SelezionaSorgenteVideo (KramerCitylife.SORGENTE_VIDEO.MYSKY);
				this.soundweb.SelezionaPresetAudio (SoundWebBlu100BSS.PRESET.MYSKY);
				break;
			case SORGENTE.APPLETV:
				this.kramer.SelezionaSorgenteVideo (KramerCitylife.SORGENTE_VIDEO.APPLETV);
				this.soundweb.SelezionaPresetAudio (SoundWebBlu100BSS.PRESET.APPLETV);
				break;
			case SORGENTE.AIRPLAY:
				this.soundweb.SelezionaPresetAudio (SoundWebBlu100BSS.PRESET.AIRPLAY);
				break;
			case SORGENTE.DIGITALE:
				this.kramer.SelezionaSorgenteVideo (KramerCitylife.SORGENTE_VIDEO.DIGITALE);
				break;
			case SORGENTE.BLURAY:
				this.kramer.SelezionaSorgenteVideo (KramerCitylife.SORGENTE_VIDEO.BLURAY);
				break;
			}
		}
	}
}