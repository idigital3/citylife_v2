﻿using System;
using iDigitalDomotica;

namespace DomoticaCitylife
{
	public class TelecomandoSky
	{
		private Telecomando telecomando;
		public enum COMANDO { NUM_0, NUM_1, NUM_2, NUM_3, NUM_4, NUM_5, NUM_6, NUM_7, NUM_8, NUM_9, ASTERISCO, CANCELLETTO, CANALE_PIU, CANALE_MENO, FRECCIA_SU, FRECCIA_GIU,
			FRECCIA_DX, FRECCIA_SX, ENTER, INFO, ROSSO, GIALLO, VERDE, BLU, GUIDE, EXIT, INDIETRO, PAUSA, PLAY, AVANTI, STOP, ONDEMAND, MYSKY, REGISTRA };
		

		public TelecomandoSky (Stanze.STANZE stanza)
		{
			switch (stanza) {
			case Stanze.STANZE.LIVING:
				telecomando = new Telecomando (Rete.LIVING_IP_PULCE_SKY, Rete.PORTA_PULCE_IR, Rete.LIVING_CANALE_PULCE_SKY);
				break;
			case Stanze.STANZE.CUCINA:
				telecomando = new Telecomando (Rete.CUCINA_IP_PULCE_SKY, Rete.PORTA_PULCE_IR, Rete.CUCINA_CANALE_PULCE_SKY);
				break;
			case Stanze.STANZE.EMANUELA:
				//Non c'è la tv
				break;
			case Stanze.STANZE.EMANUELE:
				//Non c'è sky
				break;
			}

			if (this.telecomando != null) {
				this.telecomando.ConnectTimeout = Rete.TIMEOUT;
				this.telecomando.SendTimeout = Rete.TIMEOUT;
				this.telecomando.ReceiveTimeout = Rete.TIMEOUT;
			}
		}


		public void InviaComandoIR(COMANDO comando){

			if (this.telecomando == null) return;

			string strComando = string.Empty;

			switch (comando) {

			case COMANDO.NUM_0:
				strComando = iDigitalDomotica.TelecomandoSky.ZERO;
				break;
			case COMANDO.NUM_1:
				strComando = iDigitalDomotica.TelecomandoSky.UNO;
				break;
			case COMANDO.NUM_2:
				strComando = iDigitalDomotica.TelecomandoSky.DUE;
				break;
			case COMANDO.NUM_3:
				strComando = iDigitalDomotica.TelecomandoSky.TRE;
				break;
			case COMANDO.NUM_4:
				strComando = iDigitalDomotica.TelecomandoSky.QUATTRO;
				break;
			case COMANDO.NUM_5:
				strComando = iDigitalDomotica.TelecomandoSky.CINQUE;
				break;
			case COMANDO.NUM_6:
				strComando = iDigitalDomotica.TelecomandoSky.SEI;
				break;
			case COMANDO.NUM_7:
				strComando = iDigitalDomotica.TelecomandoSky.SETTE;
				break;
			case COMANDO.NUM_8:
				strComando = iDigitalDomotica.TelecomandoSky.OTTO;
				break;
			case COMANDO.NUM_9:
				strComando = iDigitalDomotica.TelecomandoSky.NOVE;
				break;
			case COMANDO.ASTERISCO:
				strComando = "";
				break;
			case COMANDO.CANCELLETTO:
				strComando = "";
				break;
			case COMANDO.CANALE_PIU:
				strComando = iDigitalDomotica.TelecomandoSky.CANALE_PIU;
				break;
			case COMANDO.CANALE_MENO:
				strComando = iDigitalDomotica.TelecomandoSky.CANALE_MENO;
				break;
			case COMANDO.FRECCIA_SU:
				strComando = iDigitalDomotica.TelecomandoSky.FRECCIA_SU;
				break;
			case COMANDO.FRECCIA_GIU:
				strComando = iDigitalDomotica.TelecomandoSky.FRECCIA_GIU;
				break;
			case COMANDO.FRECCIA_DX:
				strComando = iDigitalDomotica.TelecomandoSky.FRECCIA_DX;
				break;
			case COMANDO.FRECCIA_SX:
				strComando = iDigitalDomotica.TelecomandoSky.FRECCIA_SX;
				break;
			case COMANDO.ENTER:
				strComando = iDigitalDomotica.TelecomandoSky.OK;
				break;
			case COMANDO.INFO:
				strComando = iDigitalDomotica.TelecomandoSky.INFO;
				break;
			case COMANDO.ROSSO:
				strComando = iDigitalDomotica.TelecomandoSky.ROSSO;
				break;
			case COMANDO.GIALLO:
				strComando = iDigitalDomotica.TelecomandoSky.GIALLO;
				break;
			case COMANDO.VERDE:
				strComando = iDigitalDomotica.TelecomandoSky.VERDE;
				break;
			case COMANDO.BLU:
				strComando = iDigitalDomotica.TelecomandoSky.BLU;
				break;
			case COMANDO.GUIDE:
				strComando = iDigitalDomotica.TelecomandoSky.GUIDA_TV;
				break;
			case COMANDO.EXIT:
				strComando = iDigitalDomotica.TelecomandoSky.ESC;
				break;
			case COMANDO.INDIETRO:
				strComando = iDigitalDomotica.TelecomandoSky.INDIETRO;
				break;
			case COMANDO.PAUSA:
				strComando = iDigitalDomotica.TelecomandoSky.PAUSA;
				break;
			case COMANDO.PLAY:
				strComando = iDigitalDomotica.TelecomandoSky.PLAY;
				break;
			case COMANDO.AVANTI:
				strComando = iDigitalDomotica.TelecomandoSky.AVANTI;
				break;
			case COMANDO.STOP:
				strComando = iDigitalDomotica.TelecomandoSky.STOP;
				break;
			case COMANDO.ONDEMAND:
				strComando = iDigitalDomotica.TelecomandoSky.ONDEMAND;
				break;
			case COMANDO.MYSKY:
				strComando = iDigitalDomotica.TelecomandoSky.MYSKY;
				break;
			case COMANDO.REGISTRA:
				strComando = iDigitalDomotica.TelecomandoSky.REGISTRA;
				break;
			}

			telecomando.InviaComandoIR (strComando);
		}
	}
}