﻿using System;

namespace iDigitalDomotica
{
	public class Telecomando : PulceIR
	{
		private enum RICHIESTA { ACCENSIONE, SPEGNIMENTO, TOGGLE, VOID }
		private RICHIESTA richiesta;
		private SensoreFFrate sensoreTV;
		private string comandoOnOff;

		public Telecomando (string ipPulce, Int32 portaPulce, uint canalePulce) : base (ipPulce, portaPulce, canalePulce)
		{
			this.sensoreTV = null;
			this.richiesta = RICHIESTA.VOID;    
		}

		public Telecomando (string ipPulce, Int32 portaPulce, uint canalePulce, string ipSensoreFrate, Int32 portaSensoreFrate, string comandoOnOff) : base(ipPulce, portaPulce, canalePulce) 
		{
			this.sensoreTV = new SensoreFFrate (ipSensoreFrate, portaSensoreFrate, LetturaSensoreAttivo, LetturaSensoreNonAttivo);
			this.richiesta = RICHIESTA.VOID;
			this.comandoOnOff = comandoOnOff;
		}

		public void Accendi(){
			if (this.sensoreTV == null) return;
			this.richiesta = RICHIESTA.ACCENSIONE;
			this.sensoreTV.Interroga ();
		}

		public void Spegni(){
			if (this.sensoreTV == null) return;
			this.richiesta = RICHIESTA.SPEGNIMENTO;
			this.sensoreTV.Interroga ();
		}

		public void ToggleStatoTV(){
			//Se la richiesta è toggle invio direttamente il comando senza interrogare il sensore
			this.richiesta = RICHIESTA.TOGGLE;
			this.InviaComandoIR (this.comandoOnOff);
		}


		/// <summary>
		/// Callback richiamata quando ottengo come risposta dal sensore che la tv è accesa
		/// </summary>
		private void LetturaSensoreAttivo(){
			switch (this.richiesta) {
			case RICHIESTA.ACCENSIONE:

				break;
			case RICHIESTA.SPEGNIMENTO:
				//Invio comando on/off
				this.InviaComandoIR (this.comandoOnOff);
				break;
			case RICHIESTA.TOGGLE:
				
				break;
			}
		}


		/// <summary>
		/// Callback richiamata quando ottengo come risposta dal sensore che la tv NON è accesa
		/// </summary>
		private void LetturaSensoreNonAttivo(){
			switch (this.richiesta) {
			case RICHIESTA.ACCENSIONE:
				//Invio comando on/off
				this.InviaComandoIR (this.comandoOnOff);
				break;
			case RICHIESTA.SPEGNIMENTO:
				
				break;
			case RICHIESTA.TOGGLE:
				
				break;
			}
		}
	}
}