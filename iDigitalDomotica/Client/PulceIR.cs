﻿using System;
using System.Threading.Tasks;
using System.Net;

namespace iDigitalDomotica
{
	public class PulceIR : iDigitalTCPClient
	{
		protected const string CHAR_CANALE = @"$ch$";
		private uint canale;
		private string comando;

		//Porta 4998

		public PulceIR (string ip, Int32 porta, uint canale) : base(IPAddress.Parse(ip), porta, false)
		{
			this.canale = canale;
			this.DataReceived += PulceIR_DataReceived;
			this.ConnectionStatusChanged += PulceIR_ConnectionStatusChanged;
		}


		//Invia il comando IR
		public void InviaComandoIR(string comando){

			this.comando = comando;

			if (string.IsNullOrWhiteSpace (this.comando)) {
				Log ("Errore, stringa comando nulla");
				return;
			}
			if (this.comando.Contains (CHAR_CANALE)) {
				this.comando = comando.Replace (CHAR_CANALE, canale.ToString());
			}
			this.Connect ();
		}



		//Evento richiamato quando cambia lo stato della connessione al socket
		private void PulceIR_ConnectionStatusChanged (iDigitalTCPClient sender, ConnectionStatus status)
		{
			switch (status) {

			case ConnectionStatus.AutoReconnecting:

				break;
			case ConnectionStatus.Connected:
				this.Send (this.comando);
				break;
			case ConnectionStatus.ConnectFail_Timeout:

				break;
			case ConnectionStatus.Connecting:

				break;
			case ConnectionStatus.DisconnectedByHost:

				break;
			case ConnectionStatus.DisconnectedByUser:

				break;
			case ConnectionStatus.Error:
				this.Disconnect ();
				break;
			case ConnectionStatus.NeverConnected:
				
				break;
			case ConnectionStatus.ReceiveFail_Timeout:
				this.Disconnect ();
				break;
			case ConnectionStatus.SendFail_NotConnected:
				this.Disconnect ();
				break;
			case ConnectionStatus.SendFail_Timeout:
				this.Disconnect ();
				break;
			}

			Log ("STATO: " + status.ToString ());
		}


		//Evento richiamato al ricevimento della risposta
		private void PulceIR_DataReceived (iDigitalTCPClient sender, object data)
		{
			this.Disconnect ();
			Log ("RISPOSTA: " + data.ToString ());
		}


		private void Log(string msg){
			#if DEBUG
				Console.WriteLine ("PulceIR: " + IP.ToString () + ":" + Port.ToString () + " CH: " + canale.ToString () + " - " + msg);
			#endif
		}
	}
}