﻿using System;
using System.Net.Sockets;
using System.Threading;

namespace iDigitalDomotica
{
	public class SensoreFFrate : iDigitalTCPClient
	{
		private string ip;
		private Int32 porta;		//5000

		public delegate void StatoSensoreAttivo();
		public delegate void StatoSensoreNonAttivo();

		private StatoSensoreAttivo callbackAttivo;
		private StatoSensoreNonAttivo callbackNonAttivo;

		public SensoreFFrate (string ip, Int32 porta, StatoSensoreAttivo callbackAttivo, StatoSensoreNonAttivo callbackNonAttivo) : base (System.Net.IPAddress.Parse(ip), porta, false)
		{
			this.ip = ip;
			this.porta = porta;
			this.callbackAttivo = callbackAttivo;
			this.callbackNonAttivo = callbackNonAttivo;
			this.DataReceived += SensoreFFrate_DataReceived;
			this.ConnectionStatusChanged += SensoreFFrate_ConnectionStatusChanged;
		}

		public void Interroga(){
			this.Connect ();
		}

		private void SensoreFFrate_ConnectionStatusChanged (iDigitalTCPClient sender, ConnectionStatus status)
		{
			switch (status) {

			case ConnectionStatus.AutoReconnecting:

				break;
			case ConnectionStatus.Connected:
				this.Send ("?TV1");
				break;
			case ConnectionStatus.ConnectFail_Timeout:
				
				break;
			case ConnectionStatus.Connecting:

				break;
			case ConnectionStatus.DisconnectedByHost:

				break;
			case ConnectionStatus.DisconnectedByUser:

				break;
			case ConnectionStatus.Error:
				this.Disconnect ();
				break;
			case ConnectionStatus.NeverConnected:

				break;
			case ConnectionStatus.ReceiveFail_Timeout:
				this.Disconnect ();
				break;
			case ConnectionStatus.SendFail_NotConnected:
				this.Disconnect ();
				break;
			case ConnectionStatus.SendFail_Timeout:
				this.Disconnect ();
				break;
			}

			Log ("STATO: " + status.ToString ());
		}



		private void SensoreFFrate_DataReceived (iDigitalTCPClient sender, object data)
		{
			try{
				
				RichiamaCallback (data.ToString ().Contains ("TV1On") ? true : false);
				Log ("RISPOSTA: " + data.ToString ());
			}
			catch(Exception ex){
				RichiamaCallback (false);
				Log ("ERRORE: " + ex.ToString ());
			}
			finally{
				this.Disconnect ();
			}
		}



		private void RichiamaCallback(bool result){
			if (result) {
				if (this.callbackAttivo != null) {
					this.callbackAttivo.Invoke ();
				}
			} else {
				if (this.callbackNonAttivo != null) {
					this.callbackNonAttivo.Invoke ();
				}
			}
		}



		private void Log(string msg){
			#if DEBUG
				Console.WriteLine ("SensoreFFrate: " + IP.ToString () + ":" + Port.ToString () + " - " + msg);
			#endif
		}



		/// <summary>
		/// Interroga il sensore
		/// </summary>
		/// <returns>Ritorna true se il sensore rileva un consumo di corrente</returns>
		public bool QueryStatus ()
		{
			TcpClient tcpClient = new TcpClient(this.ip, this.porta);
			NetworkStream stream = tcpClient.GetStream();
			byte[] bytes = System.Text.Encoding.ASCII.GetBytes("?TV1");
			stream.Write(bytes, 0, bytes.Length);
			Thread.Sleep(1000);
			byte[] numArray = new byte[tcpClient.ReceiveBufferSize];
			stream.Read(numArray, 0, tcpClient.ReceiveBufferSize);
			string risposta = System.Text.Encoding.ASCII.GetString(numArray);
			stream.Close();
			tcpClient.Close();
			return risposta.Contains("TV1On") ? true : false;
		}
	}
}