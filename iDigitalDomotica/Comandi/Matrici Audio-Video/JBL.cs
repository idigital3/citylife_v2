﻿using System;

namespace iDigitalDomotica
{
	public class JBL
	{
		/*
        public static string PowerOn = "%2310MPWR01.";
        public static string PowerOff = "%2310MPWR00.";
        public static string Source_1 = "%2310MSRC00.";
        public static string Source_2 = "%2310MSRC01.";
        public static string Source_3 = "%2310MSRC02.";
        public static string Source_4 = "%2310MSRC03.";
        public static string Source_5 = "%2310MSRC04.";
        public static string Source_6 = "%2310MSRC05.";
        public static string Source_7 = "%2310MSRC06.";
        public static string Source_8 = "%2310MSRC07.";
        public static string MuteToggle = "%2310MMUT02.";
        public static string VolumeUp = "%2310MVOLUP.";
        public static string VolumeDown = "%2310MVOLDN.";
        */



		public static string PowerOn = "#10MPWR01.";
		public static string PowerOff = "#10MPWR00.";
		public static string Source_0 = "#10MSRC00.";
		public static string Source_1 = "#10MSRC01.";
		public static string Source_2 = "#10MSRC02.";
		public static string Source_3 = "#10MSRC03.";
		public static string Source_4 = "#10MSRC04.";
		public static string Source_5 = "#10MSRC05.";
		public static string Source_6 = "#10MSRC06.";
		public static string Source_7 = "#10MSRC07.";
		public static string Source_8 = "#10MSRC08.";
		public static string MuteOff = "#10MMUT00.";
		public static string MuteOn = "#10MMUT01.";
		public static string MuteToggle = "#10MMUT02.";
		public static string VolumeUp = "#10MVOLUP.";
		public static string VolumeDown = "#10MVOLDN.";

		//Volume
		public static string Volume3dB = "#10MVOL1030.";
		public static string Volume0dB = "#10MVOL1000.";
		public static string VolumeMeno5dB = "#10MVOL0950.";
		public static string VolumeMeno10dB = "#10MVOL0900.";
		public static string VolumeMeno20dB = "#10MVOL0800.";
		public static string VolumeMeno30dB = "#10MVOL0700.";
		public static string VolumeMeno40dB = "#10MVOL0600.";
		public static string VolumeMeno50dB = "#10MVOL0500.";
		public static string VolumeMeno60dB = "#10MVOL0400.";

		public static string Stereo = "#10LMODST.";
		public static string Multicanale = "#10LMOD24.";


		//public JBL () { }
	}
}

