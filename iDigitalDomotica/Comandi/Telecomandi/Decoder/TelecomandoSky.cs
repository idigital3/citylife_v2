﻿using System;

namespace iDigitalDomotica
{
	public class TelecomandoSky : PulceIR
	{
		public static string ZERO			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,107,8,29,8,29,8,29,8,29,8,29,8,29,8,3834\r\n";
		public static string UNO			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,102,8,29,8,29,8,29,8,34,8,29,8,29,8,3834\r\n";
		public static string DUE			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,97,8,29,8,29,8,29,8,39,8,29,8,29,8,3834\r\n";
		public static string TRE			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,92,8,29,8,29,8,29,8,45,8,29,8,29,8,3834\r\n";
		public static string QUATTRO		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,86,8,29,8,29,8,29,8,50,8,29,8,29,8,3834\r\n";
		public static string CINQUE			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,81,8,29,8,29,8,29,8,55,8,29,8,29,8,3834\r\n";
		public static string SEI			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,76,8,29,8,29,8,29,8,60,8,29,8,29,8,3834\r\n";
		public static string SETTE			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38226,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,44,8,34,8,495,8,34,8,71,8,29,8,29,8,29,8,65,8,29,8,29,8,3822\r\n";
		public static string OTTO			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,66,8,29,8,29,8,29,8,71,8,29,8,29,8,3834\r\n";
		public static string NOVE			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,60,8,29,8,29,8,29,8,76,8,29,8,29,8,3834\r\n";
		public static string BLU			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,71,8,29,8,29,8,50,8,45,8,29,8,29,8,3834\r\n";
		public static string GIALLO			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38226,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,44,8,34,8,495,8,34,8,76,8,29,8,29,8,50,8,39,8,29,8,29,8,3822\r\n";
		public static string ROSSO			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,86,8,29,8,29,8,50,8,29,8,29,8,29,8,3834\r\n";
		public static string VERDE			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,81,8,29,8,29,8,50,8,34,8,29,8,29,8,3124,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,3834\r\n";
		public static string CANALE_MENO	{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,34,8,29,8,29,8,29,8,102,8,29,8,29,8,3834\r\n";
		public static string CANALE_PIU		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,39,8,29,8,29,8,29,8,97,8,29,8,29,8,3834\r\n";
		public static string ESC			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,66,8,29,8,29,8,39,8,60,8,29,8,29,8,3834\r\n";
		public static string GUIDA_TV		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,81,8,29,8,29,8,55,8,29,8,29,8,29,8,3834\r\n";
		public static string HELP			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,45,8,29,8,29,8,55,8,65,8,29,8,29,8,3834\r\n";
		public static string INFO			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,55,8,29,8,29,8,55,8,55,8,29,8,29,8,3834\r\n";
		public static string INTERATTIVO	{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,60,8,29,8,29,8,55,8,50,8,29,8,29,8,3834\r\n";
		public static string MUTO			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,36231,1,1,32,32,64,32,32,32,32,32,32,32,32,32,32,32,32,32,32,63,32,32,64,64,32,3623\r\n";
		public static string OK				{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38226,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,44,8,34,8,495,8,34,8,29,8,71,8,29,8,39,8,55,8,29,8,29,8,3822\r\n";
		public static string PRIMA_FILA		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,71,8,29,8,29,8,55,8,39,8,29,8,29,8,3834\r\n";
		public static string ULTIMO_CANALE	{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,50,8,29,8,29,8,55,8,60,8,29,8,29,8,3834\r\n";
		public static string FRECCIA_SU		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,92,8,29,8,29,8,39,8,34,8,29,8,29,8,3834\r\n";
		public static string FRECCIA_GIU	{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,45,8,71,8,29,8,39,8,39,8,29,8,29,8,3834\r\n";
		public static string FRECCIA_DX		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,76,8,29,8,29,8,39,8,50,8,29,8,29,8,3834\r\n";
		public static string FRECCIA_SX		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38226,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,44,8,34,8,495,8,34,8,81,8,29,8,29,8,39,8,44,8,29,8,29,8,3822\r\n";
		public static string MENU			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,97,8,29,8,29,8,39,8,29,8,29,8,29,8,3834\r\n";
		public static string ON_OFF			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38343,1,1,8,34,8,50,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,34,8,71,8,71,8,29,8,29,8,107,8,29,8,29,8,3834\r\n";
		public static string ONDEMAND		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,71,8,29,8,29,8,60,8,29,8,29,8,29,8,3846\r\n";
		public static string MYSKY			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,39,8,29,8,29,8,55,8,65,8,29,8,29,8,3846\r\n";
		public static string REGISTRA		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,65,8,29,8,29,8,45,8,50,8,29,8,29,8,3846\r\n";
		public static string PLAY			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,71,8,29,8,29,8,45,8,45,8,29,8,29,8,3846\r\n";
		public static string PAUSA			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,81,8,29,8,29,8,45,8,34,8,29,8,29,8,3846\r\n";
		public static string AVANTI			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,76,8,29,8,29,8,45,8,39,8,29,8,29,8,3846\r\n";
		public static string INDIETRO		{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,86,8,29,8,29,8,45,8,29,8,29,8,29,8,3846\r\n";
		public static string STOP			{ get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38461,1,1,8,39,8,45,8,29,8,107,8,50,8,50,8,45,8,34,8,496,8,39,8,60,8,29,8,29,8,45,8,55,8,29,8,29,8,3846\r\n";

		public TelecomandoSky (string ipPulce, Int32 portaPulce, uint canalePulce) : base(ipPulce, portaPulce, canalePulce) { }
	}
}