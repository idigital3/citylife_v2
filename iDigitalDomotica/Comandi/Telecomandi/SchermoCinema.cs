﻿using System;

namespace iDigitalDomotica
{
	public class SchermoCinema : PulceIR
	{
		public static string MACRO_CINEMA	{get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38226,1,1,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,685,6,280,6,185,6,280,6,185,6,280,6,185,6,280,6,185,6,185,6,185,6,185,6,3822" + "\r\n";
		public static string MACRO_16_9		{get; } = "sendir,1:" + PulceIR.CHAR_CANALE + ",1,38226,1,1,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,18,20,685,6,280,6,280,6,280,6,185,6,280,6,185,6,280,6,185,6,185,6,185,6,280,6,3822" + "\r\n";

		public SchermoCinema (string ip, Int32 porta, uint canale) : base(ip, porta, canale) { }
	}
}