// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	[Register ("Music")]
	partial class Music
	{
		[Outlet]
		UIKit.UIButton b_alloff { get; set; }

		[Outlet]
		UIKit.UIButton b_bluray { get; set; }

		[Outlet]
		UIKit.UIButton b_indietro { get; set; }

		[Outlet]
		UIKit.UIButton b_mymusic { get; set; }

		[Outlet]
		UIKit.UIButton b_spotify { get; set; }

		[Outlet]
		UIKit.UIButton b_tv_onoff { get; set; }

		[Outlet]
		UIKit.UILabel l_nomestanza { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_bluray != null) {
				b_bluray.Dispose ();
				b_bluray = null;
			}

			if (b_indietro != null) {
				b_indietro.Dispose ();
				b_indietro = null;
			}

			if (b_mymusic != null) {
				b_mymusic.Dispose ();
				b_mymusic = null;
			}

			if (b_spotify != null) {
				b_spotify.Dispose ();
				b_spotify = null;
			}

			if (l_nomestanza != null) {
				l_nomestanza.Dispose ();
				l_nomestanza = null;
			}

			if (b_tv_onoff != null) {
				b_tv_onoff.Dispose ();
				b_tv_onoff = null;
			}

			if (b_alloff != null) {
				b_alloff.Dispose ();
				b_alloff = null;
			}
		}
	}
}
