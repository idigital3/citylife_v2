// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	[Register ("Video")]
	partial class Video
	{
		[Outlet]
		UIKit.UIButton b_alloff { get; set; }

		[Outlet]
		UIKit.UIButton b_appletv { get; set; }

		[Outlet]
		UIKit.UIButton b_bluray { get; set; }

		[Outlet]
		UIKit.UIButton b_digitale { get; set; }

		[Outlet]
		UIKit.UIButton b_indietro { get; set; }

		[Outlet]
		UIKit.UIButton b_mysky { get; set; }

		[Outlet]
		UIKit.UIButton b_party { get; set; }

		[Outlet]
		UIKit.UIButton b_sky { get; set; }

		[Outlet]
		UIKit.UIButton b_tv_onoff { get; set; }

		[Outlet]
		UIKit.UILabel l_nomestanza { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_alloff != null) {
				b_alloff.Dispose ();
				b_alloff = null;
			}

			if (b_appletv != null) {
				b_appletv.Dispose ();
				b_appletv = null;
			}

			if (b_bluray != null) {
				b_bluray.Dispose ();
				b_bluray = null;
			}

			if (b_digitale != null) {
				b_digitale.Dispose ();
				b_digitale = null;
			}

			if (b_indietro != null) {
				b_indietro.Dispose ();
				b_indietro = null;
			}

			if (b_mysky != null) {
				b_mysky.Dispose ();
				b_mysky = null;
			}

			if (b_sky != null) {
				b_sky.Dispose ();
				b_sky = null;
			}

			if (b_tv_onoff != null) {
				b_tv_onoff.Dispose ();
				b_tv_onoff = null;
			}

			if (l_nomestanza != null) {
				l_nomestanza.Dispose ();
				l_nomestanza = null;
			}

			if (b_party != null) {
				b_party.Dispose ();
				b_party = null;
			}
		}
	}
}
