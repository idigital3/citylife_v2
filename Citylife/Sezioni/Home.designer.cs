// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	[Register ("Home")]
	partial class Home
	{
		[Outlet]
		UIKit.UIButton b_alloff { get; set; }

		[Outlet]
		UIKit.UIButton b_music { get; set; }

		[Outlet]
		UIKit.UIButton b_stanza_cucina { get; set; }

		[Outlet]
		UIKit.UIButton b_stanza_emanuela { get; set; }

		[Outlet]
		UIKit.UIButton b_stanza_emanuele { get; set; }

		[Outlet]
		UIKit.UIButton b_stanza_living { get; set; }

		[Outlet]
		UIKit.UIButton b_video { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_alloff != null) {
				b_alloff.Dispose ();
				b_alloff = null;
			}

			if (b_stanza_cucina != null) {
				b_stanza_cucina.Dispose ();
				b_stanza_cucina = null;
			}

			if (b_stanza_emanuela != null) {
				b_stanza_emanuela.Dispose ();
				b_stanza_emanuela = null;
			}

			if (b_stanza_emanuele != null) {
				b_stanza_emanuele.Dispose ();
				b_stanza_emanuele = null;
			}

			if (b_stanza_living != null) {
				b_stanza_living.Dispose ();
				b_stanza_living = null;
			}

			if (b_music != null) {
				b_music.Dispose ();
				b_music = null;
			}

			if (b_video != null) {
				b_video.Dispose ();
				b_video = null;
			}
		}
	}
}
