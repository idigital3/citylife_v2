﻿using UIKit;
using Xamarin;

namespace Citylife
{
	public class Application
	{
		// This is the main entry point of the application.
		static void Main (string[] args)
		{

			Insights.Initialize("38c45b715bd832d1282132ec83c16ff7d25c72c7");

			// if you want to use a different Application Delegate class from "AppDelegate"
			// you can specify it here.
			UIApplication.Main (args, null, "AppDelegate");
		}
	}
}
