using System;
using UIKit;
using CoreGraphics;

namespace Citylife
{
	public class ViewGenerica : UIView
	{
		private UIViewController parentController;
		private bool visible;

		public ViewGenerica (IntPtr h) : base(h)
		{
		}

		public ViewGenerica (UIViewController parent)
		{
			this.parentController = parent;
		}



		public bool IsVisible(){
			return this.visible;
		}


		public void show(){
			parentController.View.AddSubview (this);
			UIView.Animate (
				0.2, // duration
				() => { this.Alpha = 1f; },
				() => { visible = true; }
			);
		}

		public void hide(){
			UIView.Animate (
				0.2, // duration
				() => { this.Alpha = 0.0f; },
				() => { visible = false; this.RemoveFromSuperview(); }
			);
		}
	}
}