﻿using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;

namespace Citylife
{
	[Register("Spotify")]
	public partial class Spotify : ViewGenerica
	{
		private UIView view;
		private string url = "spotify:";


		public Spotify (IntPtr h) : base(h)
		{
		}

		public Spotify (UIViewController parent) : base(parent)
		{
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("Spotify", this, null).ValueAt(0)) as UIView;

			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);
			this.Frame = new CGRect (((float)parent.View.Bounds.Width / 2) - (view.Frame.Width / 2), ((float)parent.View.Bounds.Height / 2) - (view.Frame.Height / 2), view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			l_1.LineBreakMode = UILineBreakMode.WordWrap;
			l_1.Lines = 0;

			l_2.LineBreakMode = UILineBreakMode.WordWrap;
			l_2.Lines = 0;

			AddSubview(view);
		}


		public bool CanOpenSpotify(){
			return UIApplication.SharedApplication.CanOpenUrl (new NSUrl (url));
		}

		private void OpenSpotify(){
			if (CanOpenSpotify()) {
				UIApplication.SharedApplication.OpenUrl (new NSUrl (url));
			}
			else {
				MostraErrore ();
			}
		}

		public void MostraErrore(){
			new UIAlertView ("", "Applicazione spotify non installata", null, "OK", null).Show ();
		}
	}
}