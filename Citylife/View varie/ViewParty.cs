﻿using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;

namespace Citylife
{
	public class ViewParty : UIView
	{
		private ColonnaVolume volumeLiving;
		private ColonnaVolume volumeCucina;
		private ColonnaVolume volumeEmanuela;
		private ColonnaVolume volumeEmanuele;

		private static float width = 590;
		private static float height = 506;

		private List<ColonnaVolume> listaVolumi;
		private int indexVolume;

		private System.Timers.Timer tmrCheckLivello;

		private DomoticaCitylife.SoundWebBlu100BSS soundweb;


		public ViewParty (UIViewController parent) 
			: base (new CGRect((float)(parent.View.Frame.Width / 2) - (width / 2), (float)(parent.View.Frame.Height / 2) - (height / 2), width, height))
		{
			this.BackgroundColor = UIColor.Clear;
			this.indexVolume = 0;
			listaVolumi = new List<ColonnaVolume> ();

			float x = 0;
			volumeLiving = new ColonnaVolume (x, DomoticaCitylife.Stanze.STANZE.LIVING);
			x += 165;
			volumeCucina = new ColonnaVolume (x, DomoticaCitylife.Stanze.STANZE.CUCINA);
			x += 165;
			volumeEmanuela = new ColonnaVolume (x, DomoticaCitylife.Stanze.STANZE.EMANUELA);
			x += 165;
			volumeEmanuele = new ColonnaVolume (x, DomoticaCitylife.Stanze.STANZE.EMANUELE);

			listaVolumi.Add (volumeLiving);
			listaVolumi.Add (volumeCucina);
			listaVolumi.Add (volumeEmanuela);
			listaVolumi.Add (volumeEmanuele);

			this.AddSubview (volumeLiving);
			this.AddSubview (volumeCucina);
			this.AddSubview (volumeEmanuela);
			this.AddSubview (volumeEmanuele);

			//Controller matrice audio per attivare/disattivare filodiffusione
			soundweb = new DomoticaCitylife.SoundWebBlu100BSS();

			//Timer per schedulare le interrogazioni dei livelli volume al soundweb
			tmrCheckLivello = new System.Timers.Timer (DomoticaCitylife.Rete.TIMEOUT + 500);
			tmrCheckLivello.AutoReset = false;
			tmrCheckLivello.Elapsed += new System.Timers.ElapsedEventHandler (tmrCheckLivello_Elapsed);
		}


		//Tick del timer
		void tmrCheckLivello_Elapsed (object sender, System.Timers.ElapsedEventArgs e)
		{
			if (indexVolume < listaVolumi.Count) {
				//Console.WriteLine ("Check volume " + indexVolume);
				listaVolumi [indexVolume].CheckVolume ();
				indexVolume++;
				tmrCheckLivello.Start ();
			} else {
				tmrCheckLivello.Stop ();
				//Console.WriteLine ("Fine check volumi!!");
			}
		}


		/// <summary>
		/// Attivo la filodiffusione e avvio il timer per temporizzare le connessioni al soubdweb
		/// </summary>
		public void Inizializza(){
			soundweb.AttivaFilodiffusione ();
			indexVolume = 0;
			tmrCheckLivello.Start ();
		}


		/// <summary>
		/// Disattiva la filodiffusione e toglie la view parti dal view controller
		/// </summary>
		public void Chiudi(){
			soundweb.DisattivaFilodiffusione ();
		}
	}
}