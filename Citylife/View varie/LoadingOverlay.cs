using System;
using UIKit;
using CoreGraphics;
using System.Threading;
using Foundation;

namespace Citylife
{
	public class LoadingOverlay : UIView
	{
		private UIActivityIndicatorView spinner;
		private static int overlayWidth = 190;
		private int overlayHeight = 120;
		private UILabel label_messaggio;
		private string testoMessaggio;
		private int messaggioWidth = overlayWidth;
		private int messaggioHeight = 30;

		private System.Timers.Timer timer;

		public delegate void LoadingOnClose();
		private LoadingOnClose callback;

		private UIViewController parent;

		private int duration = 1800;

		public LoadingOverlay (UIViewController parent, LoadingOnClose callback) : base(new CGRect(0, 0, (float)parent.View.Bounds.Width, (float)parent.View.Bounds.Height))
		{
			this.parent = parent;
			this.testoMessaggio = "Attendere...";
			this.callback = callback;
			setupLayout ();

			timer = new System.Timers.Timer (duration);
			timer.AutoReset = false;
			timer.Elapsed += T_Elapsed;
		}


		public LoadingOverlay (UIViewController parent, int secondi, LoadingOnClose callback) : base(new CGRect(0, 0, (float)parent.View.Bounds.Width, (float)parent.View.Bounds.Height))
		{
			this.parent = parent;
			this.testoMessaggio = "Attendere...";
			this.callback = callback;
			this.duration = secondi;
			setupLayout ();

			timer = new System.Timers.Timer (this.duration);
			timer.AutoReset = false;
			timer.Elapsed += T_Elapsed;
		}


		private void setupLayout(){
			this.BackgroundColor = UIColor.FromRGBA (50, 50, 50, 160);

			//View centrata che contiene lo spinner
			UIView view = new UIView (new CGRect ((float)(this.Bounds.Width / 2) - (overlayWidth / 2), (float)(this.Bounds.Height / 2) - (overlayHeight / 2) - 40,
				overlayWidth, overlayHeight));
			view.BackgroundColor = UIColor.Black;
			view.Alpha = 0.85f;
			view.Layer.CornerRadius = 12.0f;

			spinner = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.WhiteLarge);
			spinner.Hidden = false;
			spinner.Frame = new CGRect(((float)this.Bounds.Width / 2) - ((float)spinner.Bounds.Width / 2), ((float)this.Bounds.Height / 2) - ((float)spinner.Bounds.Height / 2) - 55,
				(float)spinner.Bounds.Width, (float)spinner.Bounds.Height);

			label_messaggio = new UILabel(new CGRect((float)(this.Bounds.Width / 2) - (messaggioWidth / 2), (float)spinner.Frame.Y + (float)spinner.Bounds.Height + 4,
				messaggioWidth, messaggioHeight));
			label_messaggio.TextColor = UIColor.White;
			label_messaggio.BackgroundColor = UIColor.Clear;
			label_messaggio.TextAlignment = UITextAlignment.Center;
			label_messaggio.Text = testoMessaggio;

			this.Alpha = 0;

			this.AddSubview (view);
			this.AddSubview (spinner);
			this.AddSubview (label_messaggio);
		}





		public void setMessaggio(string testo){
			InvokeOnMainThread (delegate {  
				label_messaggio.Text = testo; 
			});
		}



		public void show(int msDurata){
			this.parent.View.AddSubview (this);
			UIView.Animate (
				0.3, // duration
				0.0, //delay
				UIViewAnimationOptions.TransitionCrossDissolve,
				() => { this.Alpha = 1f; },
				() => { spinner.StartAnimating(); timer.Interval = msDurata; timer.Start(); }
			);
		}


		public void show(){
			this.parent.View.AddSubview (this);
			UIView.Animate (
				0.3, // duration
				0.0, //delay
				UIViewAnimationOptions.TransitionCrossDissolve,
				() => { this.Alpha = 1f; },
				() => { spinner.StartAnimating(); timer.Interval = this.duration; timer.Start(); }
			);
		}


		public void showWithoutTimer(){
			UIView.Animate (
				0.2, // duration
				() => { this.Alpha = 1f; },
				() => { spinner.StartAnimating(); }
			);
		}


		public void hide(){
			UIView.Animate (
				0.4, // duration
				0.2, //delay
				UIViewAnimationOptions.CurveEaseOut,
				() => { this.Alpha = 0.0f; },
				() => {
					
					spinner.StopAnimating();

					if(this.callback != null){
						callback.Invoke();
					}

					this.RemoveFromSuperview();
				}
			);
		}


		//Evento tick del timer
		void T_Elapsed (object sender, System.Timers.ElapsedEventArgs e)
		{
			timer.Stop ();
			InvokeOnMainThread ( () => {
				hide ();
			});
		}
	}
}