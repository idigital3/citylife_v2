using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;

namespace Citylife
{
	[Register("MyMusic")]
	public partial class MyMusic : ViewGenerica
	{
		private UIView view;

		public MyMusic (IntPtr h) : base(h)
		{
		}

		public MyMusic (UIViewController parent) : base(parent)
		{
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("MyMusic", this, null).ValueAt(0)) as UIView;

			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);
			this.Frame = new CGRect (((float)parent.View.Bounds.Width / 2) - (view.Frame.Width / 2), ((float)parent.View.Bounds.Height / 2) - (view.Frame.Height / 2), view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			l_1.LineBreakMode = UILineBreakMode.WordWrap;
			l_1.Lines = 0;

			l_2.LineBreakMode = UILineBreakMode.WordWrap;
			l_2.Lines = 0;

			l_2.LineBreakMode = UILineBreakMode.WordWrap;
			l_2.Lines = 0;

			l_2.LineBreakMode = UILineBreakMode.WordWrap;
			l_2.Lines = 0;

			AddSubview(view);
		}
	}
}