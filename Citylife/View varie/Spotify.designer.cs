// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	partial class Spotify
	{
		[Outlet]
		UIKit.UILabel l_1 { get; set; }

		[Outlet]
		UIKit.UILabel l_2 { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (l_1 != null) {
				l_1.Dispose ();
				l_1 = null;
			}

			if (l_2 != null) {
				l_2.Dispose ();
				l_2 = null;
			}
		}
	}
}
