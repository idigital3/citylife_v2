﻿using System;
using AVFoundation;
using Foundation;
using DomoticaCitylife;
using System.Collections.Generic;
using UIKit;

namespace Citylife
{
	public class Controller
	{
		
		private Stanze.STANZE stanzaSelezionata;
		public List<UIImage> audioImages = null;

		//Player audio per suono click dei bottoni
		private AVAudioPlayer player = null;

		public Controller ()
		{
			this.stanzaSelezionata = Stanze.STANZE.LIVING;
			var url = NSUrl.FromFilename("click.mp3");
			player = AVAudioPlayer.FromUrl(url);

			//Carico tutte le immagini per la scala del volume
			this.audioImages = new List<UIImage> ();
			for(int i = 0; i < 15 + 1; i++){
				this.audioImages.Add (UIImage.FromFile ("volume_" + i.ToString() + ".png"));
			}
		}



		public void PlayClickSound(){
			try{
				if(!player.Playing){
					player.Play ();
				}	
			}
			catch(Exception ex){
				Xamarin.Insights.Report (ex);
			}
		}


		public Stanze.STANZE GetStanzaSelezionata(){
			return this.stanzaSelezionata;
		}

		public void SetStanzaSelezionata(Stanze.STANZE stanza){
			this.stanzaSelezionata = stanza;
		}
	}
}