﻿using System;

namespace Citylife
{
	public class Utility
	{
		

		//public Utility () { }


		public static float RandomX(){
			Random r = new Random(DateTime.Now.Millisecond);
			return (float)r.Next(80, 900);
		}


		public static float RandomY(){
			Random r = new Random(DateTime.Now.Millisecond);
			return (float)r.Next(80, 600);
		}

	}
}