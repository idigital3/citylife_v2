﻿using System;
using System.Threading;

namespace Citylife
{
	public class SpegniTutto
	{
		private DomoticaCitylife.TelecomandoTV telecomandoCucina;
		private DomoticaCitylife.TelecomandoTV telecomandoLiving;
		private DomoticaCitylife.TelecomandoTV telecomandoEmanuele;

		private DomoticaCitylife.SoundWebBlu100BSS soundwebCucina;
		private DomoticaCitylife.SoundWebBlu100BSS soundwebLiving;
		private DomoticaCitylife.SoundWebBlu100BSS soundwebEmanuela;
		private DomoticaCitylife.SoundWebBlu100BSS soundwebEmanuele;
		private DomoticaCitylife.SoundWebBlu100BSS soundwebEmanuelaBagno;
		private DomoticaCitylife.SoundWebBlu100BSS soundwebEmanueleBagno;

		public delegate void SpegniTuttoCompleted();
		public SpegniTuttoCompleted callback;

		public SpegniTutto (SpegniTuttoCompleted callback)
		{
			this.callback = callback;
			telecomandoCucina = new DomoticaCitylife.TelecomandoTV(DomoticaCitylife.Stanze.STANZE.CUCINA);
			telecomandoLiving = new DomoticaCitylife.TelecomandoTV(DomoticaCitylife.Stanze.STANZE.LIVING);
			telecomandoEmanuele = new DomoticaCitylife.TelecomandoTV(DomoticaCitylife.Stanze.STANZE.EMANUELE);

			soundwebCucina = new DomoticaCitylife.SoundWebBlu100BSS (DomoticaCitylife.Stanze.STANZE.CUCINA, false, false, null, MutoCucinaCompleted);
			soundwebLiving = new DomoticaCitylife.SoundWebBlu100BSS (DomoticaCitylife.Stanze.STANZE.LIVING, false, false, null, MutoLivingCompleted);
			soundwebEmanuela = new DomoticaCitylife.SoundWebBlu100BSS (DomoticaCitylife.Stanze.STANZE.EMANUELA, false, false, null, MutoEmanuelaCompleted);
			soundwebEmanuelaBagno = new DomoticaCitylife.SoundWebBlu100BSS (DomoticaCitylife.Stanze.STANZE.EMANUELA_BAGNO, false, false, null, MutoEmanuelaBagnoCompleted);
			soundwebEmanuele = new DomoticaCitylife.SoundWebBlu100BSS (DomoticaCitylife.Stanze.STANZE.EMANUELE, false, false, null, MutoEmanueleCompleted);
			soundwebEmanueleBagno = new DomoticaCitylife.SoundWebBlu100BSS (DomoticaCitylife.Stanze.STANZE.EMANUELE_BAGNO, false, false, null, MutoEmanueleBagnoCompleted);

			Esegui ();
		}





		private void Esegui(){

			//Spegnimento tv
			telecomandoCucina.SpegniTv();

			Thread.Sleep (200);

			telecomandoLiving.SpegniTv ();

			Thread.Sleep (200);

			telecomandoEmanuele.SpegniTv ();

			Thread.Sleep (200);

			//Muto zone audio
			soundwebCucina.MutoOn ();

			Thread.Sleep (500);

			soundwebLiving.MutoOn();

			Thread.Sleep (500);

			soundwebEmanuela.MutoOn();

			Thread.Sleep (500);

			soundwebEmanuelaBagno.MutoOn();

			Thread.Sleep (500);

			soundwebEmanuele.MutoOn();

			Thread.Sleep (500);

			soundwebEmanueleBagno.MutoOn();

			Thread.Sleep (3000);

			//Richiamo la callback per notificare che lo spegnimento è terminato
			this.callback.Invoke ();
		}




		//Richiamato al completamento del comando muto zona CUCINA
		private void MutoCucinaCompleted(bool isMuto, int livello){
			
		}

		//Richiamato al completamento del comando muto zona LIVING
		private void MutoLivingCompleted(bool isMuto, int livello){
			
		}

		//Richiamato al completamento del comando muto zona EMANUELA
		private void MutoEmanuelaCompleted(bool isMuto, int livello){
			
		}

		//Richiamato al completamento del comando muto zona EMANUELA BAGNO
		private void MutoEmanuelaBagnoCompleted(bool isMuto, int livello){
			
		}

		//Richiamato al completamento del comando muto zona EMANUELE
		private void MutoEmanueleCompleted(bool isMuto, int livello){
			
		}

		//Richiamato al completamento del comando muto zona EMANUELE BAGNO
		private void MutoEmanueleBagnoCompleted(bool isMuto, int livello){
			
		}
	}
}