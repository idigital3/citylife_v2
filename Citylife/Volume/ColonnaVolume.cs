﻿using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;
using DomoticaCitylife;

namespace Citylife
{
	[Register("ColonnaVolume")]
	public partial class ColonnaVolume : UIView
	{
		private UIView view;
		private UIViewController parentController;
		private SoundWebBlu100BSS soundweb;

		public ColonnaVolume (IntPtr h) : base(h)
		{
		}


		/// <summary>
		/// Costruttore utilizzato per view party
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="stanza">Stanza.</param>
		public ColonnaVolume (float x, Stanze.STANZE stanza)
		{
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("ColonnaVolume", this, null).ValueAt(0)) as UIView;
			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);

			Frame = new CGRect (x, 0, view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 1;

			l_titolo.Text = stanza.ToString ().ToLower ();

			//Eventi bottoni
			b_volumePiu.TouchUpInside += B_volumePiu_TouchUpInside;
			b_volumeMeno.TouchUpInside += B_volumeMeno_TouchUpInside;
			b_muto.TouchUpInside += B_muto_TouchUpInside;

			//Inizializzazione controller matrice audio. Flag a false per NON fare il check del volume
			soundweb = new SoundWebBlu100BSS (stanza, false, false, ComandoVolumeCompleted, ComandoMutoCompleted);

			AddSubview(view);
		}



		/// <summary>
		/// Costruttore utilizzato per volume nella sezione music e video
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="x">The x coordinate.</param>
		/// <param name="stanza">Stanza.</param>
		public ColonnaVolume (UIViewController parent, float x, Stanze.STANZE stanza, bool delayIniziale)
		{
			this.parentController = parent;
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("ColonnaVolume", this, null).ValueAt(0)) as UIView;
			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);

			float y = (float)((parent.View.Frame.Height / 2) - (view.Frame.Height / 2));

			Frame = new CGRect (x, y + 30, view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			l_titolo.Text = (stanza.ToString ().ToLower().Contains ("bagno")) ? "Bagno" : "Ambiente";

			//Eventi bottoni
			b_volumePiu.TouchUpInside += B_volumePiu_TouchUpInside;
			b_volumeMeno.TouchUpInside += B_volumeMeno_TouchUpInside;
			b_muto.TouchUpInside += B_muto_TouchUpInside;

			//Inizializzazione controller matrice audio. Flag a true per fare subito il check del volume
			soundweb = new SoundWebBlu100BSS (stanza, true, delayIniziale, ComandoVolumeCompleted, ComandoMutoCompleted);

			AddSubview(view);
		}


		//Evento bottone volume piu
		void B_volumePiu_TouchUpInside (object sender, EventArgs e)
		{
			AppDelegate.controller.PlayClickSound ();
			soundweb.VolumePiu ();
		}

		//Evento bottone volume meno
		void B_volumeMeno_TouchUpInside (object sender, EventArgs e)
		{
			AppDelegate.controller.PlayClickSound ();
			soundweb.VolumeMeno ();
		}

		//Evento bottone muto
		void B_muto_TouchUpInside (object sender, EventArgs e)
		{
			AppDelegate.controller.PlayClickSound ();
			soundweb.MutoToggle ();
		}


		public void MuteOn(){
			soundweb.MutoOn ();
		}

		public void MuteOff(bool delay){
			soundweb.MutoOff (delay);
		}

		public void CheckVolume(){
			soundweb.CheckLivelloVolume ();
		}



		//Evento richiamato quando un comando volume è stato completato con successo
		private void ComandoVolumeCompleted(int livelloVolume){
			ImpostaImmagineLivello (livelloVolume);
		}

		//Evento richiamato quando ricevo lo stato del muto
		private void ComandoMutoCompleted(bool isMuto, int livelloVolume){
			ImpostaImmagineMuto (isMuto, livelloVolume);
		}



		private void ImpostaImmagineLivello(int livello){
			try{
				i_scala.Image = AppDelegate.controller.audioImages [livello];
			}
			catch(Exception ex){
				Xamarin.Insights.Report (ex);
				//Console.WriteLine (ex.ToString ());
			}
		}


		private void ImpostaImmagineMuto(bool isMuto, int livelloVolume){
			try{

				if(isMuto){
					ImpostaImmagineLivello (0);
					b_muto.SetImage(UIImage.FromFile("audio_off.png"), UIControlState.Normal);
					b_muto.SetImage(UIImage.FromFile("audio_off_down.png"), UIControlState.Highlighted);
				}
				else {
					ImpostaImmagineLivello (livelloVolume);
					b_muto.SetImage(UIImage.FromFile("audio_on.png"), UIControlState.Normal);
					b_muto.SetImage(UIImage.FromFile("audio_on_down.png"), UIControlState.Highlighted);
				}
			}
			catch(Exception ex){
				Xamarin.Insights.Report (ex);
				//Console.WriteLine (ex.ToString ());
			}
		}



		public void show(){
			parentController.View.AddSubview (this);
			UIView.Animate (
				0.5, // duration
				0.5,
				UIViewAnimationOptions.CurveEaseInOut,
				() => { this.Alpha = 1f; },
				() => {  }
			);
		}

		public void hide(){
			UIView.Animate (
				0.5, // duration
				() => { this.Alpha = 0.0f; },
				() => { this.RemoveFromSuperview(); }
			);
		}
	}
}