// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	partial class VolumeTV
	{
		[Outlet]
		UIKit.UIButton b_muto { get; set; }

		[Outlet]
		UIKit.UIButton b_volumeMeno { get; set; }

		[Outlet]
		UIKit.UIButton b_volumePiu { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_volumePiu != null) {
				b_volumePiu.Dispose ();
				b_volumePiu = null;
			}

			if (b_volumeMeno != null) {
				b_volumeMeno.Dispose ();
				b_volumeMeno = null;
			}

			if (b_muto != null) {
				b_muto.Dispose ();
				b_muto = null;
			}
		}
	}
}
