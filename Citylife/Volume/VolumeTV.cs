﻿using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;
using DomoticaCitylife;

namespace Citylife
{
	[Register("VolumeTV")]
	public partial class VolumeTV : UIView
	{
		private UIView view;
		private UIViewController parentController;
		private TelecomandoTV telecomandoTV;

		public VolumeTV (IntPtr h) : base(h)
		{
		}


		public VolumeTV (UIViewController parent, TelecomandoTV telecomando)
		{
			this.parentController = parent;
			this.telecomandoTV = telecomando;

			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("VolumeTV", this, null).ValueAt(0)) as UIView;


			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);
			//Frame = new RectangleF ((float)(parent.View.Bounds.Width / 2) - (float)(dimensione.Width / 2), (float)(parent.View.Bounds.Height - dimensione.Height - 70) , dimensione.Width, dimensione.Height);
			Frame = new CGRect (parent.View.Bounds.Width - view.Frame.Width - (float)(view.Frame.Width * 0.8), (parent.View.Bounds.Height - view.Frame.Height - 10), view.Frame.Width, view.Frame.Height);

			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			b_volumePiu.TouchUpInside += B_volumePiu_TouchUpInside;
			b_volumeMeno.TouchUpInside += B_volumeMeno_TouchUpInside;
			b_muto.TouchUpInside += B_muto_TouchUpInside;


			AddSubview(view);
		}


		//Evento bottone volume piu
		void B_volumePiu_TouchUpInside (object sender, EventArgs e)
		{
			AppDelegate.controller.PlayClickSound ();
			telecomandoTV.VolumePiu ();
		}

		//Evento bottone volume meno
		void B_volumeMeno_TouchUpInside (object sender, EventArgs e)
		{
			AppDelegate.controller.PlayClickSound ();
			telecomandoTV.VolumeMeno ();
		}

		//Evento bottone muto
		void B_muto_TouchUpInside (object sender, EventArgs e)
		{
			AppDelegate.controller.PlayClickSound ();
			telecomandoTV.ToggleMuto ();
		}



		public void show(){
			parentController.View.AddSubview (this);
			UIView.Animate (
				0.5, // duration
				0.5,
				UIViewAnimationOptions.CurveEaseInOut,
				() => { this.Alpha = 1f; },
				() => {  }
			);
		}

		public void hide(){
			UIView.Animate (
				0.2, // duration
				() => { this.Alpha = 0.0f; },
				() => { this.RemoveFromSuperview(); }
			);
		}
	}
}