using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;

namespace Citylife
{
	[Register("TelecomandoSatellite")]
	public partial class TelecomandoSatellite : ViewGenerica
	{
		private UIView view;
		private DomoticaCitylife.TelecomandoSatellite telecomando;

		public TelecomandoSatellite (IntPtr h) : base(h)
		{
		}


		public TelecomandoSatellite (UIViewController parent) : base (parent)
		{
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("TelecomandoSatellite", this, null).ValueAt(0)) as UIView;

			//Ridimensiono la view
			//CGSize dimensione = Dettaglio.ResizeView (view);

			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);
			this.Frame = new CGRect (((float)parent.View.Bounds.Width / 2) - (view.Frame.Width / 2), ((float)parent.View.Bounds.Height / 2) - (view.Frame.Height / 2), view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			//Controller telecomando digitale terrestre
			telecomando = new DomoticaCitylife.TelecomandoSatellite (AppDelegate.controller.GetStanzaSelezionata ());


			b_ok.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.OK);
			};

			b_0.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_0);
			};

			b_1.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_1);
			};

			b_2.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_2);
			};

			b_3.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_3);
			};

			b_4.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_4);
			};

			b_5.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_5);
			};

			b_6.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_6);
			};

			b_7.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_7);
			};

			b_8.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_8);
			};

			b_9.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.NUM_9);
			};

			/*non c'è!!!
			b_cancelletto.TouchUpInside += delegate(object sender, EventArgs e) {
				
			};

			b_asterisco.TouchUpInside += delegate(object sender, EventArgs e) {
				
			};
			*/

			b_canalepiu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.CANALE_PIU);
			};

			b_canalemeno.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSatellite.COMANDO.CANALE_MENO);
			};

			AddSubview(view);
		}
	}
}