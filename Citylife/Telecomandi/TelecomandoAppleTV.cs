using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;

namespace Citylife
{
	[Register("TelecomandoAppleTV")]
	public partial class TelecomandoAppleTV : ViewGenerica
	{
		private UIView view;
		private DomoticaCitylife.TelecomandoAppleTV telecomando;

		public TelecomandoAppleTV (IntPtr h) : base(h)
		{
		}

		public TelecomandoAppleTV (UIViewController parent) : base(parent)
		{
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("TelecomandoAppleTV", this, null).ValueAt(0)) as UIView;

			//Ridimensiono la view
			//CGSize dimensione = Dettaglio.ResizeView (view);

			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);
			this.Frame = new CGRect (((float)parent.View.Bounds.Width / 2) - (view.Frame.Width / 2), ((float)parent.View.Bounds.Height / 2) - (view.Frame.Height / 2), view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			//Controller telecomando appletv
			telecomando = new DomoticaCitylife.TelecomandoAppleTV (AppDelegate.controller.GetStanzaSelezionata ());


			b_frecciasu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoAppleTV.COMANDO.FRECCIA_SU);
			};

			b_frecciagiu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoAppleTV.COMANDO.FRECCIA_GIU);
			};

			b_frecciadx.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoAppleTV.COMANDO.FRECCIA_DX);
			};

			b_frecciasx.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoAppleTV.COMANDO.FRECCIA_SX);
			};

			b_enter.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoAppleTV.COMANDO.ENTER);
			};

			b_menu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoAppleTV.COMANDO.MENU);
			};

			b_play.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoAppleTV.COMANDO.PLAY_PAUSA);
			};

			AddSubview(view);
		}
	}
}