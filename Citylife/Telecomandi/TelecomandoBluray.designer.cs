// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	partial class TelecomandoBluray
	{
		[Outlet]
		UIKit.UIButton b_enter { get; set; }

		[Outlet]
		UIKit.UIButton b_exit { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciadx { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciagiu { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciasu { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciasx { get; set; }

		[Outlet]
		UIKit.UIButton b_fwd { get; set; }

		[Outlet]
		UIKit.UIButton b_language { get; set; }

		[Outlet]
		UIKit.UIButton b_next { get; set; }

		[Outlet]
		UIKit.UIButton b_open { get; set; }

		[Outlet]
		UIKit.UIButton b_pause { get; set; }

		[Outlet]
		UIKit.UIButton b_play { get; set; }

		[Outlet]
		UIKit.UIButton b_popupmenu { get; set; }

		[Outlet]
		UIKit.UIButton b_prev { get; set; }

		[Outlet]
		UIKit.UIButton b_rew { get; set; }

		[Outlet]
		UIKit.UIButton b_stop { get; set; }

		[Outlet]
		UIKit.UIButton b_subtitles { get; set; }

		[Outlet]
		UIKit.UIButton b_topmenu { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_enter != null) {
				b_enter.Dispose ();
				b_enter = null;
			}

			if (b_exit != null) {
				b_exit.Dispose ();
				b_exit = null;
			}

			if (b_frecciadx != null) {
				b_frecciadx.Dispose ();
				b_frecciadx = null;
			}

			if (b_frecciagiu != null) {
				b_frecciagiu.Dispose ();
				b_frecciagiu = null;
			}

			if (b_frecciasu != null) {
				b_frecciasu.Dispose ();
				b_frecciasu = null;
			}

			if (b_frecciasx != null) {
				b_frecciasx.Dispose ();
				b_frecciasx = null;
			}

			if (b_fwd != null) {
				b_fwd.Dispose ();
				b_fwd = null;
			}

			if (b_language != null) {
				b_language.Dispose ();
				b_language = null;
			}

			if (b_next != null) {
				b_next.Dispose ();
				b_next = null;
			}

			if (b_open != null) {
				b_open.Dispose ();
				b_open = null;
			}

			if (b_pause != null) {
				b_pause.Dispose ();
				b_pause = null;
			}

			if (b_play != null) {
				b_play.Dispose ();
				b_play = null;
			}

			if (b_popupmenu != null) {
				b_popupmenu.Dispose ();
				b_popupmenu = null;
			}

			if (b_prev != null) {
				b_prev.Dispose ();
				b_prev = null;
			}

			if (b_rew != null) {
				b_rew.Dispose ();
				b_rew = null;
			}

			if (b_stop != null) {
				b_stop.Dispose ();
				b_stop = null;
			}

			if (b_subtitles != null) {
				b_subtitles.Dispose ();
				b_subtitles = null;
			}

			if (b_topmenu != null) {
				b_topmenu.Dispose ();
				b_topmenu = null;
			}
		}
	}
}
