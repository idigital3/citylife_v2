using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;

namespace Citylife
{
	[Register("TelecomandoBluray")]
	public partial class TelecomandoBluray : ViewGenerica
	{
		private UIView view;
		private DomoticaCitylife.TelecomandoBluray telecomando;

		public TelecomandoBluray (IntPtr h) : base(h)
		{
		}


		public TelecomandoBluray (UIViewController parent) : base(parent)
		{
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("TelecomandoBluray", this, null).ValueAt(0)) as UIView;

			//Ridimensiono la view
			//CGSize dimensione = Dettaglio.ResizeView (view);

			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);
			this.Frame = new CGRect (((float)parent.View.Bounds.Width / 2) - (view.Frame.Width / 2), ((float)parent.View.Bounds.Height / 2) - (view.Frame.Height / 2), view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			//Controller telecomando bluray
			telecomando = new DomoticaCitylife.TelecomandoBluray (AppDelegate.controller.GetStanzaSelezionata ());


			b_pause.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.PAUSA);
			};

			b_play.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.PLAY);
			};

			b_stop.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.STOP);
			};

			b_prev.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.PREV);
			};

			b_rew.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.REV);
			};

			b_fwd.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.FWD);
			};

			b_next.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.NEXT);
			};

			b_topmenu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.TOP_MENU);
			};

			b_popupmenu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.POPUP_MENU);
			};

			b_language.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.LANGUAGE);
			};

			b_subtitles.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.SUBTITLES);
			};

			b_frecciasu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.FRECCIA_SU);
			};

			b_frecciadx.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.FRECCIA_DX);
			};

			b_frecciagiu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.FRECCIA_GIU);
			};

			b_frecciasx.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.FRECCIA_SX);
			};

			b_enter.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.ENTER);
			};

			b_open.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoBluray.COMANDO.OPEN);
			};

			b_exit.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				//Comandi.EseguiComandoTelecomando(Telecomandi.COMANDO.BLURAY_EXIT);	
			};

			AddSubview(view);
		}


		public void Accendi(){
			if (this.telecomando == null) return;
			this.telecomando.InviaComandoIR (DomoticaCitylife.TelecomandoBluray.COMANDO.ACCENDI);
		}
	}
}