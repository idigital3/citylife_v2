// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	partial class TelecomandoMysky
	{
		[Outlet]
		UIKit.UIButton b_0 { get; set; }

		[Outlet]
		UIKit.UIButton b_1 { get; set; }

		[Outlet]
		UIKit.UIButton b_2 { get; set; }

		[Outlet]
		UIKit.UIButton b_3 { get; set; }

		[Outlet]
		UIKit.UIButton b_4 { get; set; }

		[Outlet]
		UIKit.UIButton b_5 { get; set; }

		[Outlet]
		UIKit.UIButton b_6 { get; set; }

		[Outlet]
		UIKit.UIButton b_7 { get; set; }

		[Outlet]
		UIKit.UIButton b_8 { get; set; }

		[Outlet]
		UIKit.UIButton b_9 { get; set; }

		[Outlet]
		UIKit.UIButton b_asterisco { get; set; }

		[Outlet]
		UIKit.UIButton b_avanti { get; set; }

		[Outlet]
		UIKit.UIButton b_blu { get; set; }

		[Outlet]
		UIKit.UIButton b_canalemeno { get; set; }

		[Outlet]
		UIKit.UIButton b_canalepiu { get; set; }

		[Outlet]
		UIKit.UIButton b_cancelletto { get; set; }

		[Outlet]
		UIKit.UIButton b_enter { get; set; }

		[Outlet]
		UIKit.UIButton b_exit { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciadx { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciagiu { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciasu { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciasx { get; set; }

		[Outlet]
		UIKit.UIButton b_giallo { get; set; }

		[Outlet]
		UIKit.UIButton b_guide { get; set; }

		[Outlet]
		UIKit.UIButton b_indietro { get; set; }

		[Outlet]
		UIKit.UIButton b_info { get; set; }

		[Outlet]
		UIKit.UIButton b_mysky { get; set; }

		[Outlet]
		UIKit.UIButton b_ondemand { get; set; }

		[Outlet]
		UIKit.UIButton b_pausa { get; set; }

		[Outlet]
		UIKit.UIButton b_play { get; set; }

		[Outlet]
		UIKit.UIButton b_registra { get; set; }

		[Outlet]
		UIKit.UIButton b_rosso { get; set; }

		[Outlet]
		UIKit.UIButton b_stop { get; set; }

		[Outlet]
		UIKit.UIButton b_verde { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_1 != null) {
				b_1.Dispose ();
				b_1 = null;
			}

			if (b_2 != null) {
				b_2.Dispose ();
				b_2 = null;
			}

			if (b_3 != null) {
				b_3.Dispose ();
				b_3 = null;
			}

			if (b_4 != null) {
				b_4.Dispose ();
				b_4 = null;
			}

			if (b_5 != null) {
				b_5.Dispose ();
				b_5 = null;
			}

			if (b_6 != null) {
				b_6.Dispose ();
				b_6 = null;
			}

			if (b_7 != null) {
				b_7.Dispose ();
				b_7 = null;
			}

			if (b_8 != null) {
				b_8.Dispose ();
				b_8 = null;
			}

			if (b_9 != null) {
				b_9.Dispose ();
				b_9 = null;
			}

			if (b_cancelletto != null) {
				b_cancelletto.Dispose ();
				b_cancelletto = null;
			}

			if (b_0 != null) {
				b_0.Dispose ();
				b_0 = null;
			}

			if (b_asterisco != null) {
				b_asterisco.Dispose ();
				b_asterisco = null;
			}

			if (b_canalepiu != null) {
				b_canalepiu.Dispose ();
				b_canalepiu = null;
			}

			if (b_canalemeno != null) {
				b_canalemeno.Dispose ();
				b_canalemeno = null;
			}

			if (b_frecciasu != null) {
				b_frecciasu.Dispose ();
				b_frecciasu = null;
			}

			if (b_frecciadx != null) {
				b_frecciadx.Dispose ();
				b_frecciadx = null;
			}

			if (b_frecciagiu != null) {
				b_frecciagiu.Dispose ();
				b_frecciagiu = null;
			}

			if (b_frecciasx != null) {
				b_frecciasx.Dispose ();
				b_frecciasx = null;
			}

			if (b_enter != null) {
				b_enter.Dispose ();
				b_enter = null;
			}

			if (b_info != null) {
				b_info.Dispose ();
				b_info = null;
			}

			if (b_rosso != null) {
				b_rosso.Dispose ();
				b_rosso = null;
			}

			if (b_giallo != null) {
				b_giallo.Dispose ();
				b_giallo = null;
			}

			if (b_verde != null) {
				b_verde.Dispose ();
				b_verde = null;
			}

			if (b_blu != null) {
				b_blu.Dispose ();
				b_blu = null;
			}

			if (b_guide != null) {
				b_guide.Dispose ();
				b_guide = null;
			}

			if (b_exit != null) {
				b_exit.Dispose ();
				b_exit = null;
			}

			if (b_indietro != null) {
				b_indietro.Dispose ();
				b_indietro = null;
			}

			if (b_pausa != null) {
				b_pausa.Dispose ();
				b_pausa = null;
			}

			if (b_play != null) {
				b_play.Dispose ();
				b_play = null;
			}

			if (b_avanti != null) {
				b_avanti.Dispose ();
				b_avanti = null;
			}

			if (b_stop != null) {
				b_stop.Dispose ();
				b_stop = null;
			}

			if (b_ondemand != null) {
				b_ondemand.Dispose ();
				b_ondemand = null;
			}

			if (b_mysky != null) {
				b_mysky.Dispose ();
				b_mysky = null;
			}

			if (b_registra != null) {
				b_registra.Dispose ();
				b_registra = null;
			}
		}
	}
}
