// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	partial class TelecomandoAppleTV
	{
		[Outlet]
		UIKit.UIButton b_enter { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciadx { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciagiu { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciasu { get; set; }

		[Outlet]
		UIKit.UIButton b_frecciasx { get; set; }

		[Outlet]
		UIKit.UIButton b_menu { get; set; }

		[Outlet]
		UIKit.UIButton b_play { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_frecciasu != null) {
				b_frecciasu.Dispose ();
				b_frecciasu = null;
			}

			if (b_frecciadx != null) {
				b_frecciadx.Dispose ();
				b_frecciadx = null;
			}

			if (b_frecciagiu != null) {
				b_frecciagiu.Dispose ();
				b_frecciagiu = null;
			}

			if (b_frecciasx != null) {
				b_frecciasx.Dispose ();
				b_frecciasx = null;
			}

			if (b_enter != null) {
				b_enter.Dispose ();
				b_enter = null;
			}

			if (b_menu != null) {
				b_menu.Dispose ();
				b_menu = null;
			}

			if (b_play != null) {
				b_play.Dispose ();
				b_play = null;
			}
		}
	}
}
