// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Citylife
{
	partial class TelecomandoSatellite
	{
		[Outlet]
		UIKit.UIButton b_0 { get; set; }

		[Outlet]
		UIKit.UIButton b_1 { get; set; }

		[Outlet]
		UIKit.UIButton b_2 { get; set; }

		[Outlet]
		UIKit.UIButton b_3 { get; set; }

		[Outlet]
		UIKit.UIButton b_4 { get; set; }

		[Outlet]
		UIKit.UIButton b_5 { get; set; }

		[Outlet]
		UIKit.UIButton b_6 { get; set; }

		[Outlet]
		UIKit.UIButton b_7 { get; set; }

		[Outlet]
		UIKit.UIButton b_8 { get; set; }

		[Outlet]
		UIKit.UIButton b_9 { get; set; }

		[Outlet]
		UIKit.UIButton b_canalemeno { get; set; }

		[Outlet]
		UIKit.UIButton b_canalepiu { get; set; }

		[Outlet]
		UIKit.UIButton b_cancelletto { get; set; }

		[Outlet]
		UIKit.UIButton b_ok { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (b_0 != null) {
				b_0.Dispose ();
				b_0 = null;
			}

			if (b_1 != null) {
				b_1.Dispose ();
				b_1 = null;
			}

			if (b_2 != null) {
				b_2.Dispose ();
				b_2 = null;
			}

			if (b_3 != null) {
				b_3.Dispose ();
				b_3 = null;
			}

			if (b_4 != null) {
				b_4.Dispose ();
				b_4 = null;
			}

			if (b_5 != null) {
				b_5.Dispose ();
				b_5 = null;
			}

			if (b_6 != null) {
				b_6.Dispose ();
				b_6 = null;
			}

			if (b_7 != null) {
				b_7.Dispose ();
				b_7 = null;
			}

			if (b_8 != null) {
				b_8.Dispose ();
				b_8 = null;
			}

			if (b_9 != null) {
				b_9.Dispose ();
				b_9 = null;
			}

			if (b_canalemeno != null) {
				b_canalemeno.Dispose ();
				b_canalemeno = null;
			}

			if (b_canalepiu != null) {
				b_canalepiu.Dispose ();
				b_canalepiu = null;
			}

			if (b_cancelletto != null) {
				b_cancelletto.Dispose ();
				b_cancelletto = null;
			}

			if (b_ok != null) {
				b_ok.Dispose ();
				b_ok = null;
			}
		}
	}
}
