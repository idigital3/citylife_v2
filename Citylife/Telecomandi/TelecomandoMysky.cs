using System;
using Foundation;
using UIKit;
using ObjCRuntime;
using CoreGraphics;
using System.Linq;

namespace Citylife
{
	[Register("TelecomandoMysky")]
	public partial class TelecomandoMysky : ViewGenerica
	{
		private UIView view;
		private DomoticaCitylife.TelecomandoSky telecomando;

		public TelecomandoMysky (IntPtr h) : base(h)
		{
		}

		public TelecomandoMysky (UIViewController parent) : base(parent)
		{
			view = Runtime.GetNSObject(NSBundle.MainBundle.LoadNib("TelecomandoMysky", this, null).ValueAt(0)) as UIView;

			//Ridimensiono la view
			//CGSize dimensione = Dettaglio.ResizeView (view);

			view.Frame = new CGRect (0, 0, view.Frame.Width, view.Frame.Height);
			this.Frame = new CGRect (((float)parent.View.Bounds.Width / 2) - (view.Frame.Width / 2), ((float)parent.View.Bounds.Height / 2) - (view.Frame.Height / 2), view.Frame.Width, view.Frame.Height);
			this.BackgroundColor = UIColor.Clear;
			this.Alpha = 0;

			//Controller telecomando mysky (è uguale a quello sky)
			telecomando = new DomoticaCitylife.TelecomandoSky (AppDelegate.controller.GetStanzaSelezionata ());


			b_0.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_0);
			};

			b_1.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_1);
			};

			b_2.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_2);
			};

			b_3.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_3);
			};

			b_4.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_4);
			};

			b_5.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_5);
			};

			b_6.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_6);
			};

			b_7.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_7);
			};

			b_8.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_8);
			};

			b_9.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.NUM_9);
			};

			b_cancelletto.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.CANCELLETTO);
			};

			b_asterisco.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.ASTERISCO);
			};

			b_canalepiu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.CANALE_PIU);
			};

			b_canalemeno.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.CANALE_MENO);
			};

			b_frecciasu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.FRECCIA_SU);
			};

			b_frecciagiu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.FRECCIA_GIU);
			};

			b_frecciadx.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.FRECCIA_DX);
			};

			b_frecciasx.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.FRECCIA_SX);
			};

			b_enter.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.ENTER);
			};

			b_info.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.INFO);
			};

			b_rosso.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.ROSSO);
			};

			b_giallo.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.GIALLO);
			};

			b_verde.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.VERDE);
			};

			b_blu.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.BLU);
			};

			b_guide.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.GUIDE);
			};

			b_exit.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.EXIT);
			};

			b_indietro.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.INDIETRO);
			};

			b_pausa.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.PAUSA);
			};

			b_play.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.PLAY);
			};

			b_avanti.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.AVANTI);
			};

			b_stop.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.STOP);
			};

			b_ondemand.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.ONDEMAND);
			};

			b_mysky.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.MYSKY);
			};

			b_registra.TouchUpInside += delegate(object sender, EventArgs e) {
				AppDelegate.controller.PlayClickSound ();
				telecomando.InviaComandoIR(DomoticaCitylife.TelecomandoSky.COMANDO.REGISTRA);
			};

			AddSubview(view);
		}
	}
}